package ru.kazakov.iteco.command.data.save;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.entity.Domain;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public final class DataSaveJaxbJsonCommand extends DataAbstractSaveCommand {

    @Getter
    @NotNull
    private final String name = "data-save-jaxb-json";

    @Getter
    @NotNull
    private final String description = "Save data in json by JAX-B.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (domainService == null) throw new Exception();
        if (!confirmed()) return;
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final String fileName = "jaxb.json";
        @NotNull final Path path = Paths.get(directory + File.separator + File.separator + fileName);
        @NotNull final Domain domain = domainService.getInstance();
        if (!Files.isDirectory(directory)) Files.createDirectory(directory);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, path.toFile());
        terminalService.write("[SAVED]");
        terminalService.write("Data successfully saved in " + fileName + "!");
        terminalService.separateLines();
    }

}
