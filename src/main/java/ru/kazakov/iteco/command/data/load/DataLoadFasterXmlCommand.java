package ru.kazakov.iteco.command.data.load;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.entity.Domain;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataLoadFasterXmlCommand extends DataAbstractLoadCommand {

    @Getter
    @NotNull
    private final String name = "data-load-faster-xml";

    @Getter
    @NotNull
    private final String description = "Load data in xml by FasterXml.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (domainService == null) throw new Exception();
        if (!confirmed()) return;
        @NotNull final String fileName = "faster.xml";
        @NotNull final Path path = Paths.get(directory + File.separator + File.separator + fileName);
        if (!Files.isDirectory(directory)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
            return;
        }
        if (!Files.exists(path)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
            return;
        }
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(path.toFile(), Domain.class);
        currentState.setCurrentUser(null);
        domainService.load(domain);
        terminalService.write("[LOADED]");
        terminalService.write("All data successfully loaded!");
        terminalService.separateLines();
    }

}
