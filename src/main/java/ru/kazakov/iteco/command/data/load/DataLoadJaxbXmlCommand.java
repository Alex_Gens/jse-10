package ru.kazakov.iteco.command.data.load;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.entity.Domain;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public final class DataLoadJaxbXmlCommand extends DataAbstractLoadCommand {

    @Getter
    @NotNull
    private final String name = "data-load-jaxb-xml";

    @Getter
    @NotNull
    private final String description = "Load data in XML by JAX-B.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (domainService == null) throw new Exception();
        if (!confirmed()) return;
        @NotNull final String fileName = "jaxb.xml";
        @NotNull final Path path = Paths.get(directory + File.separator + File.separator + fileName);
        if (!Files.isDirectory(directory)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
            return;
        }
        if (!Files.exists(path)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
            return;
        }
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(path.toFile());
        currentState.setCurrentUser(null);
        domainService.load(domain);
        terminalService.write("[LOADED]");
        terminalService.write("All data successfully loaded!");
        terminalService.separateLines();
    }

}
