package ru.kazakov.iteco.command.data.save;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.entity.Domain;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public final class DataSerializeCommand extends DataAbstractSaveCommand {

    @Getter
    @NotNull
    private final String name = "data-save-bin";

    @Getter
    @NotNull
    private final String description = "Save data in binary format.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (domainService == null) throw new Exception();
        if (!confirmed()) return;
        @NotNull final String fileName = "data.bin";
        @NotNull final Path path = Paths.get(directory + File.separator + File.separator + fileName);
        if (!Files.isDirectory(directory)) Files.createDirectory(directory);
        Files.deleteIfExists(path);
        Files.createFile(path);
        try (@NotNull final ObjectOutput stream = new ObjectOutputStream(new FileOutputStream(path.toFile()))) {
            @NotNull final Domain domain = domainService.getInstance();
            stream.writeObject(domain);
            stream.flush();
            stream.close();
            terminalService.write("[SAVED]");
            terminalService.write("Data successfully saved in " + fileName + "!");
            terminalService.separateLines();
        } catch (Exception e) {e.printStackTrace();}
    }

}
