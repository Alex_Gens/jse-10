package ru.kazakov.iteco.command.data.load;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.entity.Domain;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public final class DataDeserializeCommand extends DataAbstractLoadCommand {

    @Getter
    @NotNull
    private final String name = "data-load-bin";

    @Getter
    @NotNull
    private final String description = "Load data in binary format.";

    public boolean isAdmin() {return admin;}

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (domainService == null) throw new Exception();
        if (!confirmed()) return;
        @NotNull final String fileName = "data.bin";
        @NotNull final Path path = Paths.get(directory + File.separator + File.separator + fileName);
        if (!Files.isDirectory(directory)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
            return;
        }
        if (!Files.exists(path)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
            return;
        }
        try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(path.toFile()))) {
            @NotNull final Domain domain = (Domain) stream.readObject();
            stream.close();
            currentState.setCurrentUser(null);
            domainService.load(domain);
            terminalService.write("[LOADED]");
            terminalService.write("All data successfully loaded!");
            terminalService.separateLines();
        } catch (Exception e) {e.printStackTrace();}
    }

}
