package ru.kazakov.iteco.command.data.save;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.entity.Domain;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataSaveFasterJsonCommand extends DataAbstractSaveCommand {

    @Getter
    @NotNull
    private final String name = "data-save-faster-json";

    @Getter
    @NotNull
    private final String description = "Save data in json by FasterXml.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (domainService == null) throw new Exception();
        if (!confirmed()) return;
        @NotNull final String fileName = "faster.json";
        @NotNull final Path path = Paths.get(directory + File.separator + File.separator + fileName);
        @NotNull final Domain domain = domainService.getInstance();
        if (!Files.isDirectory(directory)) Files.createDirectory(directory);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValue(path.toFile(), domain);
        terminalService.write("[SAVED]");
        terminalService.write("Data successfully saved in " + fileName + "!");
        terminalService.separateLines();
    }

}
