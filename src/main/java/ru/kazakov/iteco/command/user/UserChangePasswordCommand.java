package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.Password;

@NoArgsConstructor
public final class UserChangePasswordCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-change-password";

    @Getter
    @NotNull
    private final String description = "Change profile password.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (userService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final User currentUser = currentState.getCurrentUser();
        @Nullable User user = currentState.getCurrentUser();
        if (currentUser.getRoleType() == RoleType.ADMINISTRATOR) {
            terminalService.write("Enter user's login to change profile password.   [" +
                    user.getRoleType().getDisplayName().toUpperCase() + "]");
            terminalService.write("ENTER LOGIN: ");
            @NotNull final String login = terminalService.enterIgnoreEmpty();
            boolean isExist = userService.contains(login);
            if (!isExist) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("User with that login doesn't exist.");
                terminalService.separateLines();
                return;
            }
            user = userService.findByLogin(login);
        }
        if (user == null) throw new Exception();
        if (currentUser.getRoleType() != RoleType.ADMINISTRATOR) {
            checkOldPassword(user);
        }
        @NotNull String firstPassword = "";
        @NotNull String secondPassword = "";
        while (true) {
            terminalService.write("ENTER NEW PASSWORD: ");
            firstPassword = terminalService.enterIgnoreEmpty();
            terminalService.write("Confirm you password.");
            terminalService.write("ENTER NEW PASSWORD: ");
            secondPassword = terminalService.enterIgnoreEmpty();
            if (!firstPassword.equals(secondPassword)) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("Entered passwords are different.");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[UPDATED]");
            break;
        }
        terminalService.write("Password updated!");
        terminalService.separateLines();
        @NotNull  final String newPassword = Password.getHashedPassword(firstPassword);
        user.setPassword(newPassword);
    }

    private void checkOldPassword(@Nullable final User user) throws Exception {
        if (user == null) throw new Exception();
        if (user.getPassword() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        terminalService.write("ENTER PASSWORD: ");
        @NotNull final String entered = terminalService.enterIgnoreEmpty();
        @NotNull final String enteredPassword = Password.getHashedPassword(entered);
        @NotNull final String oldPassword = user.getPassword();
        if (!enteredPassword.equals(oldPassword)) {
            terminalService.write("[NOT CORRECT]");
            terminalService.separateLines();
        }
    }

}
