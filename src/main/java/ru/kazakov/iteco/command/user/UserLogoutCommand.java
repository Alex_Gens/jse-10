package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserLogoutCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-logout";

    @Getter
    @NotNull
    private final String description = "Logout from account.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        currentState.setCurrentUser(null);
        terminalService.write("[LOGOUT]");
        terminalService.write("You logout from your account.");
        terminalService.separateLines();
    }

}
