package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.util.Password;

@NoArgsConstructor
public final class UserLoginCommand extends UserAbstractCommand{

    private final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "user-login";

    @Getter
    @NotNull
    private final String description = "User authorization.";

    @Override
    public boolean isSecure() {return secure;}

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (userService == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        terminalService.write("ENTER LOGIN: ");
        @NotNull final String login = terminalService.enterIgnoreEmpty();
        if (!userService.contains(login)) {
            terminalService.write("[NOT CORRECT]");
            terminalService.write("Login doesn't exist.");
            terminalService.separateLines();
            return;
        }
        @Nullable final User currentUser = currentState.getCurrentUser();
        if (currentUser != null &&
                login.equals(currentUser.getLogin())) {
            terminalService.write("[NOT CORRECT]");
            terminalService.write("You are already authorized.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[CORRECT]");
        terminalService.separateLines();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new Exception();
        if (user.getPassword() == null) throw new Exception();
        terminalService.write("ENTER PASSWORD: ");
        @NotNull String password = Password.getHashedPassword(terminalService.enterIgnoreEmpty());
        if (!user.getPassword().equals(password)) {
            terminalService.write("[NOT CORRECT]");
            terminalService.write("Incorrect password.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[CORRECT]");
        currentState.setCurrentUser(user);
        terminalService.write("Welcome " + user.getLogin() + "!" + " ["
                + user.getRoleType().getDisplayName().toUpperCase() + "]");
        terminalService.separateLines();
    }

}
