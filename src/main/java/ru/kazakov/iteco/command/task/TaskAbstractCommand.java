package ru.kazakov.iteco.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.entity.Task;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@NoArgsConstructor
public abstract class TaskAbstractCommand  extends AbstractCommand {

    @Nullable
    protected ITaskService taskService;

    @Override
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

    @Nullable
    protected Task getTaskByPart(@NotNull final String currentUserId) throws Exception {
        if (terminalService == null) throw new Exception();
        if (taskService == null) throw new Exception();
        terminalService.write("ENTER PART OF TASK NAME OR DESCRIPTION: ");
        @NotNull final String part = terminalService.enterIgnoreEmpty();
        terminalService.separateLines();
        @Nullable final List<Task> tasksByName = taskService.findAllByName(part, currentUserId);
        @Nullable final List<Task> tasksByInfo = taskService.findAllByInfo(part, currentUserId);
        if (tasksByName == null) throw new Exception();
        if (tasksByInfo == null) throw new Exception();
        if (tasksByName.isEmpty() && tasksByInfo.isEmpty()) {
            terminalService.write("Task doesn't exist. Use \"task-list\" to show all tasks.");
            terminalService.separateLines();
            return null;
        }
        int counter = 1;
        @NotNull final Map<String, Task> tasksByNameMap = new TreeMap<>();
        @NotNull final Map<String, Task> tasksByInfoMap = new TreeMap<>();
        for (Task task : tasksByName) {
            tasksByNameMap.put(String.valueOf(counter), task);
            counter++;
        }
        for (Task task : tasksByInfo) {
            tasksByInfoMap.put(String.valueOf(counter), task);
            counter++;
        }
        @NotNull final Task task;
        while (true) {
            if (!tasksByNameMap.isEmpty()) terminalService.write("Found by name: ");
            tasksByNameMap.forEach((k, v) -> terminalService.write(k + ". " + v.getName()));
            if (!tasksByInfoMap.isEmpty()) terminalService.write("Found by description:");
            tasksByInfoMap.forEach((k, v) -> terminalService.write(k + ". " + v.getName()));
            terminalService.separateLines();
            terminalService.write("Select task.");
            terminalService.write("ENTER TASK NUMBER: ");
            @NotNull final String number = terminalService.enterIgnoreEmpty();
            if (!tasksByNameMap.containsKey(number) && !tasksByInfoMap.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of item doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            if (tasksByNameMap.containsKey(number)) {
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                task = tasksByNameMap.get(number);
                break;
            }
            if (tasksByInfoMap.containsKey(number)) {
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                task = tasksByInfoMap.get(number);
                break;
            }
        }
        return task;
    }

}
