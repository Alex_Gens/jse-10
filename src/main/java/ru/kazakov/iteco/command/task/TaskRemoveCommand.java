package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;

@NoArgsConstructor
public final class TaskRemoveCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-remove";

    @Getter
    @NotNull
    private final String description = "Remove task.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        @Nullable final Task task = getTaskByPart(currentUserId);
        if (task == null) return;
        taskService.remove(task.getId());
        terminalService.write("[REMOVED]");
        terminalService.write("Task successfully removed!");
        terminalService.separateLines();
    }

}
