package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@NoArgsConstructor
public final class TaskListCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-list";

    @Getter
    @NotNull
    private final String description = "Show all tasks.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String currentUserId = currentState.getCurrentUser().getId();
        if (taskService.isEmptyRepository(currentUserId)) {
            terminalService.write("Task list is empty. Use \"task-create\" to create task.");
            terminalService.separateLines();
            return;
        }
        @NotNull final SortType[] types = SortType.values();
        Map<String, SortType> sortTypes = new TreeMap<>();
        for (int i = 0; i < types.length; i++) {
            sortTypes.put(String.valueOf(i + 1), types[i]);
        }
        terminalService.write("Select list sorting type");
        @NotNull String number;
        while (true) {
            sortTypes.forEach((key, value) -> terminalService.write(key + ". " + value.getDisplayName()));
            terminalService.separateLines();
            terminalService.write("ENTER SORTING TYPE NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!sortTypes.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of sorting type doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.separateLines();
            break;
        }
        @Nullable final List<Task> tasks = taskService.findAll(currentUserId, sortTypes.get(number));
        if (tasks == null) throw new Exception();
        terminalService.write("[TASKS LIST]");
        int counter = 1;
        for (Task task : tasks) {
            terminalService.write(counter + ". " + task.getName());
            counter++;
        }
        terminalService.separateLines();
    }

}
