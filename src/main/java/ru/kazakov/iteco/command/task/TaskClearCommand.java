package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class TaskClearCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-clear";

    @Getter
    @NotNull
    private final String description = "Remove all tasks.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String currentUserId = currentState.getCurrentUser().getId();
        taskService.removeAll(currentUserId);
        terminalService.write("[ALL TASKS REMOVED]");
        terminalService.write("Tasks successfully removed!");
        terminalService.separateLines();
    }

}
