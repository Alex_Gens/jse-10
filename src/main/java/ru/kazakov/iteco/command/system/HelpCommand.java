package ru.kazakov.iteco.command.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    private final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "help";

    @Getter
    @NotNull
    private final String description = "Show all commands.";

    @Override
    public boolean isSecure() {return secure;}

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        @Nullable final User currentUser = currentState.getCurrentUser();
        @NotNull final List<AbstractCommand> commands = currentState.getCommands();
        commands.sort(Comparator.comparing(AbstractCommand::getName));
        if (currentUser != null && currentUser.getRoleType() ==  RoleType.ADMINISTRATOR) {
            terminalService.write("[ADMINISTRATOR COMMANDS]");
            commands.forEach(v -> {if (v.isAdmin()) terminalService.write(v.getName() + ": " + v.getDescription());});
            terminalService.separateLines();
        }
        terminalService.write("[DEFAULT COMMANDS]");
        commands.forEach(v -> {if (!v.isAdmin()) terminalService.write(v.getName() + ": " + v.getDescription());});
        terminalService.separateLines();
    }

}
