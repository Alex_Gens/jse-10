package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.entity.Project;

@NoArgsConstructor
public final class ProjectCreateCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-create";

    @Getter
    @NotNull
    private final String description = "Create new project.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        terminalService.write("ENTER PROJECT NAME: ");
        @NotNull final String name = terminalService.enterIgnoreEmpty();
        if (projectService.contains(name, currentUserId)) {
            terminalService.write("[NOT CREATED]");
            terminalService.write("Project with this name is already exists. Use another project name.");
            terminalService.separateLines();
            return;
        }
        @NotNull final Project project = new Project(currentUserId);
        project.setName(name);
        projectService.persist(project);
        terminalService.write("[CREATED]");
        terminalService.write("Project successfully created!");
        terminalService.separateLines();
    }

}
