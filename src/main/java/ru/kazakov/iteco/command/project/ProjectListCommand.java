package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.*;

@NoArgsConstructor
public final class ProjectListCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-list";

    @Getter
    @NotNull
    private final String description = "Show all projects.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String currentUserId = currentState.getCurrentUser().getId();
        if (projectService.isEmptyRepository(currentUserId)) {
            terminalService.write("Project list is empty. Use \"project-create\" to create project.");
            terminalService.separateLines();
            return;
        }
        @NotNull final SortType[] types = SortType.values();
        Map<String, SortType> sortTypes = new TreeMap<>();
        for (int i = 0; i < types.length; i++) {
            sortTypes.put(String.valueOf(i + 1), types[i]);
        }
        terminalService.write("Select list sorting type");
        @NotNull String number;
        while (true) {
            sortTypes.forEach((key, value) -> terminalService.write(key + ". " + value.getDisplayName()));
            terminalService.separateLines();
            terminalService.write("ENTER SORTING TYPE NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!sortTypes.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of sorting type doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.separateLines();
            break;
        }
        @Nullable final List<Project> projects = projectService.findAll(currentUserId, sortTypes.get(number));
        if (projects == null) throw new Exception();
        terminalService.write("[PROJECTS LIST]");
        int counter = 1;
        for (Project project : projects) {
            terminalService.write(counter + ". " + project.getName());
            counter++;
            }
        terminalService.separateLines();
    }

}
