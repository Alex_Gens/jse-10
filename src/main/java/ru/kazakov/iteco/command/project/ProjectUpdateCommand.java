package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.Status;
import ru.kazakov.iteco.util.DateUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

@NoArgsConstructor
public final class ProjectUpdateCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-update";

    @Getter
    @NotNull
    private final String description = "Update project information.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        @Nullable final Project project = getProjectByPart(currentUserId);
        if (project == null) return;
        Map<String, String> items = new HashMap<>();
        items.put(String.valueOf(1), "Project start date");
        items.put(String.valueOf(2), "Project finish date");
        items.put(String.valueOf(3), "Project status");
        items.put(String.valueOf(4), "Project information");
        @NotNull String number;
        while (true) {
            terminalService.write("Select item to update: ");
            items.forEach((k, v) -> terminalService.write(k + ": " + v));
            terminalService.separateLines();
            terminalService.write("ENTER ITEM NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!items.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of item doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.separateLines();
            break;
        }
        switch (number) {
            case "1" : updateDateStart(project); return;
            case "2" : updateDateFinish(project); return;
            case "3" : updateStatus(project); return;
            case "4" : updateInformation(project);
        }
    }

    private void updateDateStart(@NotNull final Project project) throws Exception {
        if (terminalService == null) throw new Exception();
        while (true) {
        terminalService.write("Enter new project's start date in dd.mm.yyyy format.");
        terminalService.write("ENTER START DATE:");
        @NotNull final String enteredDate = terminalService.enterIgnoreEmpty();
        @Nullable final Date date = DateUtil.parseDate(enteredDate);
        if (date == null) {
            terminalService.write("[NOT UPDATED]");
            terminalService.write("Incorrect date or format.");
            terminalService.separateLines();
            continue;
        }
        project.setDateStart(date);
        terminalService.write("[UPDATED]");
        terminalService.write("Project successfully updated!");
        terminalService.separateLines();
        break;
        }
    }


    private void updateDateFinish(@NotNull final Project project) throws Exception {
        if (terminalService == null) throw new Exception();
        while (true) {
            terminalService.write("Enter new project's start date in dd.mm.yyyy format.");
            terminalService.write("ENTER FINISH DATE:");
            @NotNull final String enteredDate = terminalService.enterIgnoreEmpty();
            @Nullable final Date date = DateUtil.parseDate(enteredDate);
            if (date == null) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("Incorrect date or format.");
                terminalService.separateLines();
                continue;
            }
            project.setDateFinish(date);
            terminalService.write("[UPDATED]");
            terminalService.write("Project successfully updated!");
            terminalService.separateLines();
            break;
        }
    }

    private void updateStatus(@NotNull final Project project) throws Exception {
        if (terminalService == null) throw new Exception();
        @NotNull final Map<String, Status> statusMap = new TreeMap<>();
        @NotNull final Status[] statuses = Status.values();
        for (int i = 0; i < statuses.length; i++) {
            statusMap.put(String.valueOf(i + 1), statuses[i]);
        }
        terminalService.write("Select status:");
        @NotNull String number;
        while (true) {
            statusMap.forEach((key, value) -> terminalService.write(key + ". " + value.getDisplayName()));
            terminalService.separateLines();
            terminalService.write("ENTER STATUS NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!statusMap.containsKey(number)) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("This number of status doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            project.setStatus(statusMap.get(number));
            terminalService.write("[UPDATED]");
            terminalService.write("Project successfully updated!");
            terminalService.separateLines();
            break;
        }
    }

    private void updateInformation(@NotNull final Project project) throws Exception {
        if (terminalService == null) throw new Exception();
        terminalService.write("Use \"-save\" to finish entering, and save information.");
        terminalService.write("ENTER PROJECT INFORMATION: ");
        @NotNull final String newInfo = terminalService.read();
        project.setInfo(newInfo);
        terminalService.write("[UPDATED]");
        terminalService.write("Project successfully updated!");
        terminalService.separateLines();
    }

}
