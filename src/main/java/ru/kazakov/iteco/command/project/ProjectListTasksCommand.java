package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@NoArgsConstructor
public final class ProjectListTasksCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-list-tasks";

    @Getter
    @NotNull
    private final String description = "Show all tasks in project.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        @Nullable final Project project = getProjectByPart(currentUserId);
        if (project == null) return;
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final SortType[] types = SortType.values();
        Map<String, SortType> sortTypes = new TreeMap<>();
        for (int i = 0; i < types.length; i++) {
            sortTypes.put(String.valueOf(i + 1), types[i]);
        }
        terminalService.write("Select list sorting type");
        @NotNull String number;
        while (true) {
            sortTypes.forEach((key, value) -> terminalService.write(key + ". " + value.getDisplayName()));
            terminalService.separateLines();
            terminalService.write("ENTER SORTING TYPE NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!sortTypes.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of sorting type doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.separateLines();
            break;
        }
        @Nullable final List<Task> tasks = taskService.findAll(project.getId(), currentUserId, sortTypes.get(number));
        if (tasks == null) throw new Exception();
        if (tasks.isEmpty()) {
            terminalService.write("The project has no tasks. Use project-add-task to add task to project.");
            terminalService.separateLines();
            return;
        }
        int counter = 1;
        terminalService.write("[TASKS LIST]");
        for (Task task : tasks) {
            terminalService.write(counter + ". " + task.getName());
            counter++;
        }
        terminalService.separateLines();
    }

}
