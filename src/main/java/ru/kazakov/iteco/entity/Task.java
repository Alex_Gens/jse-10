package ru.kazakov.iteco.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.Status;
import java.util.Date;

@Data
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @NotNull
    private String userId;

    @Nullable
    private String projectId;

    @Nullable
    private String name = "";

    @Nullable
    private String info = "";

    @Nullable
    private Date dateStart = new Date();

    @Nullable
    private Date dateFinish = new Date();

    @NotNull
    private Status status = Status.PLANNED;

    public Task(@NotNull final String userId) {this.userId = userId;}

    public boolean isEmpty() {return info == null || info.isEmpty();}

}
