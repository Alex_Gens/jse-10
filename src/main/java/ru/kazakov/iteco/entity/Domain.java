package ru.kazakov.iteco.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@JsonAutoDetect
@NoArgsConstructor
public final class Domain implements Serializable {

    @Setter
    @Nullable
    private List<Project> projects;

    @Setter
    @Nullable
    private List<Task> tasks;

    @Setter
    @Nullable
    private List<User> users;


    @Nullable
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    public List<Project> getProjects() {return this.projects;}

    @Nullable
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    public List<Task> getTasks() {return this.tasks;}

    @Nullable
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    public List<User> getUsers() {return this.users;}

}
