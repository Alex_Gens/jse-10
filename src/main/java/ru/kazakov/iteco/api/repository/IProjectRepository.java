package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    public String getName(@NotNull final String id);

    public void remove(@NotNull final List<String> ids);

    public void removeAll(@NotNull final String currentUserId);

    @Nullable
    public Project findByName(@NotNull final String name);

    @Nullable
    public Project findByName(@NotNull final String name, @NotNull final String currentUserId);

    @NotNull
    public List<Project> findAll(@NotNull final List<String> ids);

    @NotNull
    public List<Project> findAll(@NotNull final String currentUserId);

    @NotNull
    public List<Project> findAll(@NotNull final String currentUserId, @NotNull final SortType sortType);

    @NotNull
    public List<Project>  findAllByName(@NotNull final String part, @NotNull final String currentUserId);

    @NotNull
    public List<Project>  findAllByInfo(@NotNull final String part, @NotNull final String currentUserId);

    public boolean contains(@NotNull final String name);

    public boolean contains(@NotNull final String name, @NotNull final String currentUserId);

    public boolean isEmpty(@NotNull final String id);

    public boolean isEmptyRepository(@NotNull final String currentUserId);

}
