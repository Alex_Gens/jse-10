package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.List;

public interface IRepository<T> {

    public void merge(@NotNull final T entity) throws Exception;

    public void persist(@NotNull final T entity) throws Exception;

    public void remove(@NotNull final String id) throws Exception;

    public void removeAll();

    @Nullable
    public T findOne(@NotNull final String id) throws Exception;

    @NotNull
    public List<T> findAll();

    public boolean isEmptyRepository();

}
