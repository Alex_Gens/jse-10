package ru.kazakov.iteco.api.context;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.api.service.*;

public interface ServiceLocator {

    @NotNull
    public ITaskService getTaskService();

    @NotNull
    public IProjectService getProjectService();

    @NotNull
    public IUserService getUserService();

    @NotNull
    public ITerminalService getTerminalService();

    @NotNull
    public IDomainService getDomainService();

}
