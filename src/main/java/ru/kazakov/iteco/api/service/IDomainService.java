package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Domain;

public interface IDomainService {

    @NotNull
    public Domain getInstance() throws Exception;

    public void load(@Nullable final Domain domain) throws Exception;

}
