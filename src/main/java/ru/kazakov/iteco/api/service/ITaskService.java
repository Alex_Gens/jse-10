package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    public String getName(@Nullable final String id) throws Exception;

    @Nullable
    public String getProjectId(@Nullable final String id) throws Exception;

    public void setProjectId(@Nullable final String id, @Nullable final String projectId) throws Exception;

    public void remove(@Nullable final List<String> ids) throws Exception;

    public void removeWithProject(@Nullable final String currentUserId, @Nullable final String projectId) throws Exception;

    public void removeAll(@Nullable final String currentUserId) throws Exception;

    public void removeAllWithProjects(@Nullable final String currentUserId) throws Exception;

    @Nullable
    public Task findByName(@Nullable final String name) throws Exception;

    @Nullable
    public Task findByName(@Nullable final String name, @Nullable final String currentUserId) throws Exception;

    @Nullable
    public List<Task> findAll(@Nullable final List<String> ids) throws Exception;

    @Nullable
    public List<Task> findAll(@Nullable final String currentUserId) throws Exception;

    @Nullable
    public List<Task> findAll(@Nullable final String currentUserId, @Nullable final SortType sortType) throws Exception;

    @Nullable
    public List<Task> findAll(@Nullable final String projectId,
                              @Nullable final String currentUserId,
                              @Nullable final SortType sortType) throws Exception;

    @Nullable
    public List<Task> findAllByName(@Nullable final String part, @Nullable final String currentUserId) throws Exception;

    @Nullable
    public List<Task> findAllByInfo(@Nullable final String part, @Nullable final String currentUserId) throws Exception;

    public boolean contains(@Nullable final String name) throws Exception;

    public boolean contains(@Nullable final String name, @Nullable final String currentUserId) throws Exception;

    public boolean isEmpty(@Nullable final String id) throws Exception;

    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception;

}
