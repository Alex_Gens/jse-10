package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.NotNull;
import java.io.IOException;

public interface ITerminalService {

    public String enterIgnoreEmpty() throws IOException;

    @NotNull
    public String read() throws IOException;

    public void write(@NotNull String text);

    public void separateLines();

}
