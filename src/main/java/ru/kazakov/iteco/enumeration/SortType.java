package ru.kazakov.iteco.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public enum SortType {

    CREATED("Created"),
    START("Start date"),
    FINISH("Finish date"),
    STATUS("Status");

    @Getter
    @NotNull
    private String displayName;

}
