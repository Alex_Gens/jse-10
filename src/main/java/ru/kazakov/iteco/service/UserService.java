package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.User;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IUserRepository repository;

    public UserService(@NotNull final IUserRepository userRepository) {this.repository = userRepository;}

    @Nullable
    @Override
    public String getName(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.getName(id);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        repository.remove(id);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        return repository.findByLogin(login);
    }

    @Override
    public boolean contains(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        return repository.contains(login);
    }

    @NotNull
    @Override
    protected IUserRepository getRepository() {return repository;}

}
