package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    private final ITaskRepository repository;

    public TaskService(@NotNull ITaskRepository repository) {this.repository = repository;}

    @Nullable
    @Override
    public String getName(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.getName(id);
    }

    @Nullable
    @Override
    public String getProjectId(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.getProjectId(id);
    }

    @Override
    public void setProjectId(@Nullable final String id, @Nullable final String projectId) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        repository.setProjectId(id, projectId);
    }

    @Override
    public void remove(@Nullable final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        repository.remove(ids);
    }

    @Override
    public void removeWithProject(@Nullable final String currentUserId, @Nullable final String projectId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        repository.removeWithProject(currentUserId, projectId);
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.removeAll(currentUserId);
    }

    @Override
    public void removeAllWithProjects(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.removeAllWithProjects(currentUserId);
    }

    @Nullable
    @Override
    public Task findByName(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.findByName(name);
    }

    @Nullable
    @Override
    public Task findByName(@Nullable final String name, @Nullable final String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findByName(name, currentUserId);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        return repository.findAll(ids);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findAll(currentUserId);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String currentUserId, @Nullable final SortType sortType) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        return repository.findAll(currentUserId, sortType);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String projectId,
                              @Nullable final String currentUserId,
                              @Nullable final SortType sortType) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        return repository.findAll(projectId, currentUserId, sortType);
    }

    @Nullable
    @Override
    public List<Task> findAllByName(@Nullable final String part, @Nullable final String currentUserId) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findAllByName(part, currentUserId);
    }

    @Nullable
    @Override
    public List<Task> findAllByInfo(@Nullable final String part, @Nullable final String currentUserId) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findAllByInfo(part, currentUserId);
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.contains(name);
    }

    @Override
    public boolean contains(@Nullable final String name, @Nullable final String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.contains(name, currentUserId);
    }

    @Override
    public boolean isEmpty(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.isEmpty(id);
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.isEmptyRepository(currentUserId);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository() {return repository;}

}
