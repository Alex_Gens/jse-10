package ru.kazakov.iteco.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.Domain;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.entity.User;
import java.util.List;

@RequiredArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IUserService userService;

    @NotNull
    public Domain getInstance() throws Exception {
        @NotNull final Domain domain = new Domain();
        @Nullable final List<Project> projects = projectService.findAll();
        @Nullable final List<Task> tasks = taskService.findAll();
        @Nullable final List<User> users = userService.findAll();
        if (projects == null) throw new Exception();
        if (tasks == null) throw new Exception();
        if (users == null) throw new Exception();
        domain.setProjects(projects);
        domain.setTasks(tasks);
        domain.setUsers(users);
        return domain;
    }

    @Override
    public void load(@Nullable final Domain domain) throws Exception {
        if (domain == null) throw new Exception();
        @Nullable final List<Project> projects = domain.getProjects();
        @Nullable final List<Task> tasks = domain.getTasks();
        @Nullable final List<User> users = domain.getUsers();
        if (projects == null) throw new Exception();
        if (tasks == null) throw new Exception();
        if (users == null) throw new Exception();
        projectService.removeAll();
        taskService.removeAll();
        userService.removeAll();
        for (Project project : projects) {projectService.persist(project);}
        for (Task task : tasks) {taskService.persist(task);}
        for (User user : users) {userService.persist(user);}
    }

}
