package ru.kazakov.iteco.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.entity.User;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public String getName(@NotNull final String id) {return entities.get(id).getName();}

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return entities.values().stream()
                .filter(v -> v.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    @Override
    public boolean contains(@NotNull final String login) {
        return entities.values().stream()
                .anyMatch(v -> v.getLogin().equals(login));
    }

}
